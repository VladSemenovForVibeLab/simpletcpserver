import org.junit.Test;
import ru.semenov.steps.two.ApplicationContext;
import ru.semenov.steps.two.SupportManager;

import static org.junit.Assert.assertNotNull;

public class ApplicationContextTest {
    @Test
    public void contextLoads() {
        ApplicationContext context = new ApplicationContext();
        assertNotNull(context.getInstance(SupportManager.class));
    }

}
