package ru.semenov.steps.one;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

public class TomcatEssentials {
    private static final int PORT = 8082;
    private static final int N_THREADS = 12;
    private static final ExecutorService executor = Executors.newFixedThreadPool(N_THREADS);
    private static final BlockingQueue<Socket> connectionQueue = new LinkedBlockingQueue<>();
    public static void main(String[] args){
        try (ServerSocket serverSocket = new ServerSocket(PORT)){
            for (int i = 0; i< N_THREADS; i++){
                executor.submit(()->{
                    while (true){
                        Socket client = connectionQueue.take();
                        connectionQueue.put(client);
                    }
                });
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
