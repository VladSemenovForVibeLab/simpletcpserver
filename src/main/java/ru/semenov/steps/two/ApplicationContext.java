package ru.semenov.steps.two;

import lombok.SneakyThrows;
import org.reflections.Reflections;
import ru.semenov.steps.two.configuration.Bean;
import ru.semenov.steps.two.configuration.Configuration;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class ApplicationContext {
    private final Map<Class<?>, Object> instances = new HashMap<>();

    @SneakyThrows
    public ApplicationContext(){
        Reflections reflections = new Reflections("ru.semenov.steps.two.configuration");
        Set<Object> configurations = reflections.getTypesAnnotatedWith(Configuration.class)
                .stream()
                .map(type -> {
                    try {
                        return type.getDeclaredConstructor().newInstance();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toSet());
        for (Object configuration: configurations){
            Set<Method> methods = Arrays.stream(configuration.getClass().getMethods())
                    .filter(method -> method.isAnnotationPresent(Bean.class))
                    .collect(Collectors.toSet());
            for(Method method: methods){
                instances.put(method.getReturnType(), method.invoke(configuration));
            }
        }
    }
    public <T> T getInstance(Class<T> type){
        return (T) Optional.ofNullable(instances.get(type));
    }
}
