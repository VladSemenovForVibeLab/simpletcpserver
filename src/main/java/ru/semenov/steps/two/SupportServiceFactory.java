package ru.semenov.steps.two;

public class SupportServiceFactory {
    private static volatile SupportServiceImpl INSTANCE;
    public static SupportServiceImpl getInstance() {
        if (INSTANCE == null) {
            synchronized (SupportServiceImpl.class){
                if (INSTANCE == null)
                    INSTANCE = new SupportServiceImpl();
            }
        }
        return INSTANCE;
    }
}
