package ru.semenov.steps.two.configuration;

import ru.semenov.steps.two.SupportManager;
import ru.semenov.steps.two.SupportService;
import ru.semenov.steps.two.SupportServiceImpl;

@Configuration
public class SupportConfiguration {
    @Bean
    public SupportManager supportManager(){
        return new SupportManager(supportService());
    }
    @Bean
    public SupportService supportService(){
        return new SupportServiceImpl();
    }
}
