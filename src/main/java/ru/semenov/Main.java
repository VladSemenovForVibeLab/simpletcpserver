package ru.semenov;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        int port = 8080;
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Сервер запущен. Слушается порт: "+port);
            while (true){
                Socket clientSocket = serverSocket.accept();
                System.out.println("Клиент подключился с "+clientSocket.getInetAddress());
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),true);
                String inputLine = in.readLine();
                System.out.println("От клиента "+inputLine);
                System.out.println("out "+out);
                System.out.println("in "+out);
                parseRawHttpRequest(inputLine);
                clientSocket.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private static void parseRawHttpRequest(String rawRequest) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new StringReader(rawRequest));
        String line = bufferedReader.readLine();
        String[] requestLine = line.split(" ");
        String method = requestLine[0];
        String path = requestLine[1];
        String version = requestLine[2];
        System.out.println(method + "\n" + path +"\n" + version);
        Map<String, String> headers = new HashMap<String, String>();
        while ((line = bufferedReader.readLine()) != null
        && !line.isEmpty()){
            String[] header = line.split(": ");
            headers.put(header[0],header[1]);
        }
        StringBuilder body = new StringBuilder();
        while((line = bufferedReader.readLine()) !=null){
            body.append(line).append("\n");
        }
    }
}